﻿namespace OobAPI

open System
open System.Reflection
open BepInEx
open HarmonyLib
open OobAPI.Hooks
open OobAPI.Plugin
open TMPro
open UnityEngine

type BasePlugin(info: PluginInfo) =
    interface OobPlugin with    
        override this.Init () =
            Debug.Log("Hello from Oob Plugin!")
            MainMenuDrawHook.EVENT.Register(this)

    interface MainMenuDrawHook.Callback with
        override this.OnDraw (instance) =
            let font = instance.GetComponentInChildren<TMP_Text>().font

            let button = GameObject("apiLabel")
            button.transform.SetParent(instance.transform, false)
            button.transform.position <- Vector3(0.0f, 0.5f, 1.0f)

            let text = button.AddComponent<TextMeshProUGUI>()
            text.text <- String.Format("oobapi {0}", info.Metadata.Version)
            text.color <- Color.white
            text.alignment <- TextAlignmentOptions.Center
            text.font <- font

            (button.transform :?> RectTransform).sizeDelta <- Vector2(500.0f, 50.0f)
            text.outlineWidth <- 0.13f
            text.outlineColor <- Color32(94uy, 59uy, 137uy, 255uy)

[<BepInPlugin("org.generalprogramming.ooblets.OobAPI", "OobAPI", "1.0.0.0")>]
[<BepInProcess("ooblets.exe")>]
type OobAPIPlugin() =
    inherit BaseUnityPlugin()
    
    member this.Awake() =
        this.Logger.LogInfo("Patching oob!")
        
        let harmony = Harmony("org.generalprogramming.ooblets.OobAPI")
        harmony.PatchAll(Assembly.GetExecutingAssembly())
        
        PluginLoader.loadPlugin(BasePlugin(this.Info))
