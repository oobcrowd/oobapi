﻿namespace OobAPI.Hooks

open OobAPI.Event

module MainMenuDrawHook =
    type Callback =
        abstract OnDraw : UI_NewMainMenu -> unit

    let EVENT = EventFactory.create(fun listeners ->
        { new Callback with
            member this.OnDraw (instance) = for l in listeners do l.OnDraw(instance) })
