﻿namespace OobAPI.Hooks

open OobAPI.Event

module GameResumeHook =
    type Callback =
        abstract OnGameResume : unit -> unit

    let EVENT = EventFactory<Callback>.create(fun listeners ->
        { new Callback with
            member _.OnGameResume () = for l in listeners do l.OnGameResume() })

module SceneLoadedHook =
    type Callback =
        abstract OnSceneLoaded : SceneTag -> unit

    let EVENT = EventFactory<Callback>.create(fun listeners ->
        { new Callback with
            member _.OnSceneLoaded (tag) = for l in listeners do l.OnSceneLoaded(tag) })
