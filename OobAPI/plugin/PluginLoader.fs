﻿namespace OobAPI.Plugin

module PluginLoader =
    let mutable allPlugins: OobPlugin list = []
    
    let loadPlugin (pl : OobPlugin) =
        allPlugins <- pl :: allPlugins

module OobPluginInitalizer =
    let rec initializeAll: OobPlugin list -> unit = function
        | (pl :: tail) ->
            pl.Init()
            initializeAll(tail)
        | [] -> ()

    let initialize () =
        initializeAll(PluginLoader.allPlugins)
