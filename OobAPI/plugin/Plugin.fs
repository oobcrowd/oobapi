﻿namespace OobAPI.Plugin

type OobPlugin =
    abstract member Init : unit -> unit
