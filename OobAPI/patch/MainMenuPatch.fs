﻿namespace OobAPI.Patch

open HarmonyLib
open OobAPI.Hooks

[<HarmonyPatch(typeof<UI_NewMainMenu>, "RefreshMainMenuButtons")>]
type MainMenuPatch =
    static member Postfix (__instance: UI_NewMainMenu) =
        MainMenuDrawHook.EVENT.invoker.OnDraw(__instance)