﻿namespace OobAPI.Patch

open HarmonyLib
open OobAPI.Hooks

module StateTransitionHelper =
    let handleStateTransition (state: GameManager.GameState) =
        match state with
        | GameManager.GameState.normal -> GameResumeHook.EVENT.invoker.OnGameResume()
        | _ -> ()

[<HarmonyPatch(typeof<GameManager>, "TransitionState")>]
type StateTransitionPatch =
    static member Prefix(__0: GameManager.GameState, ___currentState: GameManager.GameState, __state: outref<bool>) =
        __state <- __0 <> ___currentState

    static member Postfix (__0: GameManager.GameState, __state: inref<bool>) =
        if __state then
            StateTransitionHelper.handleStateTransition(__0)
