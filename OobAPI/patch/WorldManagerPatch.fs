﻿namespace OobAPI.Patch

open HarmonyLib
open OobAPI.Hooks

[<HarmonyPatch(typeof<WorldManager>, "SceneWasLoaded")>]
type SceneLoadedPatch =
    static member Postfix (__0: SceneTag) =
        SceneLoadedHook.EVENT.invoker.OnSceneLoaded(__0)
