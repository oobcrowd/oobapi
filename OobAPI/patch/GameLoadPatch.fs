﻿namespace OobAPI.Patch

open System.Collections
open TMPro
open HarmonyLib
open OobAPI.Plugin
open OobAPI.Util
open UnityEngine

module GameLoaderHelper =
    let loadMods (loadingText: TMP_Text): seq<obj> = seq {
        loadingText.text <- "Loading mods :)"
        yield WaitForSeconds(0.25f)
        OobPluginInitalizer.initialize()
        yield ()
    }

[<HarmonyPatch(typeof<GameLoader>, "LoadServices")>]
type GameLoaderPatch =
    static member Postfix (orig: IEnumerator, ___loadingText: TMP_Text) =
        seq {
            yield! Coroutines.compose(orig)
            yield! GameLoaderHelper.loadMods(___loadingText)
        } :?> IEnumerator
