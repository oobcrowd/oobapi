﻿namespace OobAPI.Util

open System.Collections

module Coroutines =
    let rec compose (enum: IEnumerator): seq<obj> = seq {
        if enum.MoveNext() then
            yield enum.Current
            yield! compose(enum)
    }
