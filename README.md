# OobAPI

Ooblets modding & compatibility API.

Still in early stages, but you can make some basic mods!

## Developer Usage

You can check out our current example mod [EnableGotsapon](https://gitlab.com/oobcrowd/enablegotsapon)
which is small enough to fit in a single file and should be easy to understand.

We hope to have a C# example soon as well.
